const Discord = require('discord.js')

function DiscordCallbacks (data, post_data) {
  if (!data.access_token) {
    throw new TypeError('No access token specified')
  }
  
  this.access_token = data.access_token
  this.discord_client = new Discord.Client()
  
  this.discord_client.on('error', e => this.onDiscordError(e))
  
  this.post_data = post_data
}

DiscordCallbacks.prototype.start = function () {
  return this.discord_client.login(this.access_token)
}

DiscordCallbacks.prototype.onImageUpdate = function () {
  if (this.post_data && this.post_data.image) {
    var message = this.post_data.image.message
    var imagedata = this.post_data.image.data
    
    try {
      console.log('Sending Discord messages...')
      
      this.discord_client.channels
        .filter(channel => channel.type === 'text')
        .forEach((channel) => {
          try {
            channel.send(message, new Discord.Attachment(imagedata))
            console.log('Sent Discord message in', channel.name)
          } catch (error) {
            console.error('Failed to send a Discord message in',
              channel.name, error)
          }
        })
    } catch (error) {
      console.error('Failed to send any Discord messages', error)
    }
  }
}

DiscordCallbacks.prototype.onSchoolAlert = function (alert) {
  if (alert) {
    try {
      console.log('Sending Discord messages...')
      
      this.discord_client.channels
        .filter(channel => channel.type === 'text')
        .forEach((channel) => {
          try {
            channel.send(alert)
            console.log('Sent Discord message in', channel.name)
          } catch (error) {
            console.error('Failed to send a Discord message in',
              channel.name, error)
          }
        })
    } catch (error) {
      console.error('Failed to send any Discord messages', error)
    }
    return
  }
  return Promise.reject(new Error('No alert data provided'))
}

DiscordCallbacks.prototype.onDiscordError = function (error) {
  console.error('Dickscord encountered an error', error)
}

module.exports = DiscordCallbacks
