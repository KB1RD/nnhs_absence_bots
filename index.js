#!/usr/bin/env node

const cron = require('node-cron')
const fetch = require('node-fetch')
const request = require('request').defaults({ encoding: null })

const htmlparser2 = require("htmlparser2")

const chalk = require("chalk")
const figlet = require("figlet")

const fs = require('fs')

var program = require('commander')

const Validator = require('jsonschema').Validator
const schema = require('./schema.json')

const package = require('./package.json')

program
  .version(package.version)
  .option('-c, --config <location>', 'Set the configuration location. Defaults to ./config.json.', './config.json')
  .parse(process.argv)

// Loaded configuration
var config = {}

// Location where fetched data is stored
var postData = {
}

var pushers = []

function fetchInstagramPictures (name) {
  return fetch('https://www.instagram.com/' + encodeURIComponent(name))
    .then(res => res.text())
    .then((str) => {
      var object = undefined
     
      // Instagram embeds its JSON object in a script
      var re = new RegExp("<script type\=\\\"text/javascript\\\">(.*?)</script>", "gmi")
      while (res = re.exec(str)) {
        
        // Because regex is slow, I actually need to parse this in two parts
        // to limit backtracking
        var re2 = new RegExp(/window\._sharedData[ ]?=[ ]?(.*)[;\n]{1}/, "gmi")
        while (res2 = re2.exec(res[1])) {
          // Now, try to parse and assign the object
          try {
            if (!object) {
              object = JSON.parse(res2[1])
            }
          } catch (e) {
            console.error(chalk.red('Error parsing JSON. Ignoring'), e)
          }
        }
      }
      
      // Now, extract the image data
      var pictures = []
      
      // The profiles are sorted out in an array fairly early on, so this is
      // split into a seperate function to make it easier to read...
      function forEachProfile(obj) {
        if (obj && obj.graphql && obj.graphql.user &&
            obj.graphql.user.edge_owner_to_timeline_media &&
            obj.graphql.user.edge_owner_to_timeline_media.edges &&
            Array.isArray(
              obj.graphql.user.edge_owner_to_timeline_media.edges)) {
          obj.graphql.user.edge_owner_to_timeline_media.edges.forEach((obj) => {
            if (obj.node && obj.node.__typename &&
                (obj.node.__typename === 'GraphImage' ||
                obj.node.__typename === 'GraphSidecar') &&
                obj.node.display_url && obj.node.taken_at_timestamp) {
              pictures.push(obj.node)
            }
          })
        }
      }
      
      // Now, use the above code to process the available profiles
      if (object && object.entry_data && object.entry_data.ProfilePage &&
          Array.isArray(object.entry_data.ProfilePage)) {
        object.entry_data.ProfilePage.forEach(forEachProfile)
      }
      
      return pictures
    })
}

function fetchTodaysPictureFromPictureList (pictures) {
  var todays_latest_pic
  
  // Sort through each picture
  pictures.forEach((pic) => {
    var date = new Date(pic.taken_at_timestamp * 1000)
    var today = new Date()
    
    // Find the date of the latest pic for reference
    var latest_date
    if (todays_latest_pic) {
      latest_date = new Date(todays_latest_pic.taken_at_timestamp * 1000)
    }
    
    // Now, compare the dates: 1.) Was the image posted today?
    // 2.) Is it actually after the last one?
    if (date.setHours(0,0,0,0) === today.setHours(0,0,0,0) &&
        (!latest_date || date > latest_date)) {
      todays_latest_pic = pic
    }
  })
  
  return todays_latest_pic
}

// A cache of the last processed timestamp so we know if the image is new or old
var last_image_timestamp
function pictureProcessingJob() {
  fetchInstagramPictures(config.source.account)
  .then(fetchTodaysPictureFromPictureList)
  .then((image) => {
    if (!image) {
      throw new Error('There is no image available.')
    }
    
    if (!image.dimensions || !image.taken_at_timestamp || !image.display_url) {
      throw new Error('The image object is invalid')
    }
    
    if (last_image_timestamp === image.taken_at_timestamp) {
      // Nothing to do; Let's not spam the user
      console.log(chalk.yellow('No new images. The latest one has already been sent'))
      return
    }
    last_image_timestamp = image.taken_at_timestamp
    
    // If it's already been posted, change the message
    var message = 'Today\'s absence list'
    if (last_image_timestamp &&
        new Date(last_image_timestamp).setHours(0,0,0,0) ==
        new Date().setHours(0,0,0,0)) {
      message = 'Today\'s absence list has been updated'
    }
    
    console.log('Fetching image...')
    
    request.get(image.display_url, (err, res, body) => {
      if(err) {
        throw err
      }
      
      console.log('Found image. Sending to chat rooms...')
      
      if (res && res.headers && res.headers['content-type'] &&
          res.headers['content-length']) {
        // Update the shared data object
        postData.image = {
          w: image.dimensions.width,
          h: image.dimensions.height,
          mime: res.headers['content-type'],
          size: res.headers['content-length'],
          data: body,
          message
        }
        
        // Notify the pushers
        pushers.forEach(pusher => {
          try {
            const promise = pusher.onImageUpdate()
            if (promise) {
              promise.catch(e => console.log(chalk.red('Pusher threw error'), e))
            }
          } catch (e) {
            console.log(chalk.red('Pusher threw error'), e)
          }
        })
      } else {
        throw new Error('The returned image data was invalid')
      }
    })
  })
  .catch((error) => {
    console.error(chalk.red('Failed to fetch new image information'), error)
  })
}

function fetchAlerts() {
  return fetch(config.alert_source.url)
    .then(res => res.text())
    .then((body) => {
      const alerts = []
      let waiting_for_alerts = false
      let current_alert = undefined
      const parser = new htmlparser2.Parser(
        {
          onopentag(name, attribs) {
            if (name === 'ul' && attribs.id === 'onscreenalertdialoglist') {
              waiting_for_alerts = true
            } else if (name === 'li' && waiting_for_alerts) {
              current_alert = ''
            }
          },
          ontext(text) {
            if (waiting_for_alerts) {
              current_alert += text + ' '
            }
          },
          onclosetag(tagname) {
            if (tagname === 'ul') {
              waiting_for_alerts = false
            } else if (tagname === 'li' && waiting_for_alerts) {
              alerts.push(current_alert.trim())
            }
          }
        },
        { decodeEntities: true }
      )
      parser.write(body)
      parser.end()
      return alerts
    })
}

let last_alert
async function alertProcessingJob() {
  const alert = (await fetchAlerts()).join('\n')

  if (last_alert === alert) {
    // Don't re-post
    return
  }
  last_alert = alert

  // Notify the pushers
  pushers.forEach(pusher => {
    try {
      const promise = pusher.onSchoolAlert(alert)
      if (promise) {
        promise.catch(e => console.log(chalk.red('Pusher threw error'), e))
      }
    } catch (e) {
      console.log(chalk.red('Pusher threw error'), e)
    }
  })
}

function readConfig(config_loc) {
  if (!config_loc) {
    console.error(chalk.red('No configuration specified'))
    return
  }
  
  try {
    return JSON.parse(fs.readFileSync(config_loc, 'utf8'))
  } catch (e) {
    console.error(chalk.red('Failed to load configuration'), e)
    return
  }
}

function validateSchema(config, schema) {
  var v = new Validator()
  
  v.customFormats.cron = cron.validate
  
  const result = v.validate(config, schema)
  if (result.errors && result.errors.length) {
    throw result.errors
  }
  
  return true
}

function main() {
  config = readConfig(program.config)
  if(!config) {
    return false
  }

  console.log(
    chalk.blue(
      figlet.textSync("NNHS Absence Bots")
    )
  )

  try {
    validateSchema(config, schema)
  } catch (e) {
    console.error(chalk.red('Configuration is invalid'), e)
    return false
  }
  
  Object.keys(config.pushers).forEach(key => {
    try {
      const pusher = require(key)
      if (pusher) {
        pushers.push(new pusher(config.pushers[key], postData))
      }
    } catch (e) {
      console.log(chalk.red('Error creating pusher ' + key), e)
    }
  })

  console.log('Starting absence bots...')
  Promise.all(pushers.map(pusher => {
    try {
      const promise = pusher.start()
      if (promise) {
        promise.catch(error => {
          console.error(chalk.red('Failed to start pusher'), error)
          pushers.pop(pusher)
        })
        return promise
      }
      return Promise.resolve(null)
    } catch (e) {
      console.error(chalk.red('Failed to start pusher'), error)
      pushers.pop(pusher)
      return Promise.reject(e)
    }
  }))
  // Run from 7AM to 3PM every two minutes from Monday to Friday
  .then(() => cron.schedule(config.source.poll_interval || '*/2 7-15 * * monday-friday', pictureProcessingJob))
  .then(() => cron.schedule(config.alert_source.poll_interval || '*/5 6-22 * * monday-friday', alertProcessingJob))
  .then(() => console.log(chalk.green('Absence bots started!')))
  .catch(e => {
    console.error(chalk.red('Failed to start'), e)
    process.exit(1)
  })
  return true
}

if (!main()) {
  process.exit(1)
}
