# NNHS Absence Bots

Bots for Matrix and Discord that post my school absence list from Instagram (where a friend posts them) and posts them on various social media platforms. This is a single-purpose bot and the code quality is more or less horrible. I know. It just works and that's all I care about right now since it's so simple.

## Example Configuration

```json
{
  "source": {
    "account": "nnhsabsence",
    "poll_interval": "*/2 7-15 * * monday-friday"
  },
  "alert_source": {
    "url": "https://url/to/schoolwires/OnScreenAlertDialogListWrapper.aspx",
    "poll_interval": "*/5 6-22 * * monday-friday"
  },
  "pushers": {
    "./matrix_pusher.js": {
      "mxid": "@mybot:matrix.org",
      "hs": "https://matrix.org/",
      "access_token": "fdjfsldjflsdkj"
    },
    "./discord_pusher.js": {
      "access_token": "sdfjdslkfjsdlkjf"
    }
    "./twitter_pusher.js": {
      "consumer_key": "fsdfsdfsdf",
      "consumer_secret": "fsdfsdfsdf",
      "access_token": "fsdfsdfsdf",
      "access_token_secret": "fsdfsdfsdf"
    }
  }
}
```
