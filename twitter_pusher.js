const Twit = require('twit')

function TwitterCallbacks ({ consumer_key, consumer_secret, access_token, access_token_secret }, post_data) {
  if (!consumer_key) {
    throw new TypeError('No consumer key specified')
  }
  if (!consumer_secret) {
    throw new TypeError('No consumer secret specified')
  }
  if (!access_token) {
    throw new TypeError('No access token specified')
  }
  if (!access_token_secret) {
    throw new TypeError('No access token secret specified')
  }
  
  this.twit = new Twit({ consumer_key, consumer_secret, access_token, access_token_secret })
  
  this.post_data = post_data
}

TwitterCallbacks.prototype.start = function () {}

TwitterCallbacks.prototype.onImageUpdate = async function () {
  if (this.post_data && this.post_data.image) {
    var image = this.post_data.image
    var message = image.message
    var imagedata = image.data

    const { data } = await this.twit.post('media/upload', { media_data: imagedata.toString('base64') })

    await this.twit.post('statuses/update', {
      status: message || '',
      media_ids: [
        data.media_id_string
      ]
    })
    return
  }
  await Promise.reject(new Error('No image data provided'))
}

TwitterCallbacks.prototype.onSchoolAlert = function (alert) {
  if (alert) {
    return this.twit.post('statuses/update', { status: alert })
  }
  return Promise.reject(new Error('No alert data provided'))
}

module.exports = TwitterCallbacks
