const Matrix = require('matrix-js-sdk')

function MatrixCallbacks (data, post_data) {
  if (!data.access_token) {
    throw new TypeError('No access token specified')
  }
  if (!data.mxid) {
    throw new TypeError('No user ID specified')
  }
  if (!data.hs) {
    throw new TypeError('No homeserver URL specified')
  }
  
  this.matrix_client = Matrix.createClient({
       baseUrl: data.hs,
       accessToken: data.access_token,
       userId: data.mxid
    })
  
  var matrix_client = this.matrix_client
  this.matrix_client.on("RoomMember.membership", function(event, member) {
    if (member.membership === "invite" && member.userId === data.mxid) {
      matrix_client.joinRoom(member.roomId).done(function () {
        console.log("Matrix: auto-joined %s", member.roomId)
        if (post_data.image) {
          this.sendImageTo(member.roomId)
        }
      })
    }
  })
  
  this.post_data = post_data
}

MatrixCallbacks.prototype.start = function () {
  return this.matrix_client.startClient()
}

MatrixCallbacks.prototype.sendImageTo = function (room) {
  if (this.post_data && this.post_data.image && this.post_data.image.mx_url) {
    var image = this.post_data.image
    var message = image.message
    var imagedata = image.data
    
    var content = {
      "body": message,
      "info": {
        "w": image.w,
        "h": image.h,
        "mimetype": image.mime,
        "size": image.size
      },
      "msgtype": "m.image",
      "url": image.mx_url
    }
    return this.matrix_client.sendEvent(room, "m.room.message", content)
      .catch((error) => {
        console.error('Failed to send message', error)
      })
  }
  
  return Promise.reject(new Error('No image data provided'))
}

MatrixCallbacks.prototype.onImageUpdate = function () {
  if (this.post_data && this.post_data.image) {
    var image = this.post_data.image
    var message = image.message
    var imagedata = image.data
    
    return Promise.all([this.matrix_client.getJoinedRooms()
        .then((rooms) => { return rooms.joined_rooms }),
        this.matrix_client.uploadContent(imagedata, 
          {type: image.mime, rawResponse: false})
      ])
      .then((data) => {
        console.log('Got Matrix joined rooms and uploaded image', data[0])
        // It is assumed that valid imagedata is passed in...
        image.mx_url = data[1].content_uri
        data[0].forEach((room) => this.sendImageTo(room))
      })
      .catch((error) => {
        console.error('Matrix failed to send message to all rooms', error)
      })
  }
  return Promise.reject(new Error('No image data provided'))
}

MatrixCallbacks.prototype.onSchoolAlert = function (alert) {
  if (alert) {
    return this.matrix_client.getJoinedRooms()
      .then((rooms) => { return rooms.joined_rooms })
      .then((rooms) => {
        console.log('Got Matrix joined rooms')
        rooms.forEach((room) => {
          var content = {
            "body": alert,
            "msgtype": "m.notice"
          }
          return this.matrix_client.sendEvent(room, "m.room.message", content)
            .catch((error) => {
              console.error('Failed to send message', error)
            })
        })
      })
      .catch((error) => {
        console.error('Matrix failed to send message to all rooms', error)
      })
  }
  return Promise.reject(new Error('No alert data provided'))
}

module.exports = MatrixCallbacks
